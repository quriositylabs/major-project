import React from 'react';
import Form from './components/Form.jsx';
import axios from 'axios';
import forms from './store/forms';
import commanders from './store/commanders';

export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.formData = forms.client;
		this.disabled = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
		this.values = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
		this.gotData = false;
		this.state = { modified: false };
	}

	componentDidMount() {
    	// $('[data-toggle="tooltip"]').tooltip(); 
    	$('#clientModal').modal('show');
	}

	getAlias(data, buttonEvent, clearForm) {
		this.gotData = true;
		axios({
				method: 'post',
				url: 'selectServer',
				data: {
					alias: data[0],
					passphrase: data[1]
				},
			})
			.then((response) => {
				if(response.status == 200) {
					let commented = response.data;
					this.disabled = ['disabled', 'disabled', 'disabled', '', '', '', '', '', '', '', '', 'disabled', 'disabled', 'disabled', '', ''];
					this.values = [commented[1], commented[2], commented[0], '', '', '', '', '', '', '', '', commented[3], commented[4], commented[5], '', ''];
					$('#clientModal').modal('hide');
					this.setState({ modified: true });
					// clearForm();
				}
			})
			.catch((error) => {
			    console.log(error);
			});
	}

	sendDetails(data, values, buttonEvent, clearForm) {
		if (this.gotData || data.length == 0) {
			if (data.length == 0) {
				data = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
			}
			data[0] = commanders.Interface('', this.values[0]);
			if (this.values[1].length != 0) {
				data[1] = commanders.Text('dev\-node .*', this.values[1]);
			}
			data[2] = commanders.Proto('', this.values[2]);
			data[11] = commanders.Boolean('tls\-.*', this.values[11]);
			data[12] = commanders.CipherClient('', this.values[12]);
			data[13] = commanders.Boolean('comp\-.*', this.values[13]);
		}
		console.log(data);
		axios({
				method: 'post',
				url: 'client',
				data: {
					commands: data,
					password: this.props.route.getPassword()[0],
					username: this.props.route.getPassword()[1],
				},
			})
			.then((response) => {
				if(response.status == 200) {
					alert('success');
					// clearForm();
				}
			})
			.catch((error) => {
			    console.log(error);
			});
	}

	render() {
		return (
			<div>
				<div id="clientModal" className="modal fade" role="dialog">
				  <div className="modal-dialog">
				    <div className="modal-content">
				      <div className="modal-body">
				        <Form disabled={['', '']} values={['', '']} handler={this.getAlias.bind(this)} data={forms.clientModal}></Form>
				      </div>
				    </div>
				  </div>
				</div>
				<center><h3>Client</h3></center>
				<Form disabled={this.disabled} values={this.values} data={this.formData} handler={this.sendDetails.bind(this)}></Form>
			</div>
		);
	}
}

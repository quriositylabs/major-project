import React from 'react';
import Form from './components/Form.jsx';
import axios from 'axios';
import forms from './store/forms';

export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.formData = forms.server;
		this.alias = '';
		this.aliasPassword = '';
		this.passphrase = '';
		this.disabled = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
		this.values = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
	}

	componentDidMount() {
    	// $('[data-toggle="tooltip"]').tooltip(); 
    	$('#serverModal').modal('show');
	}

	getAlias(data, buttonEvent, clearForm) {
		this.alias = data[0];
		this.aliasPassword = data[1];
		this.passphrase = (data.length == 3) ? data[2] : '';
		$('#serverModal').modal('hide');
	}

	sendDetails(data, values, buttonEvent, clearForm) {
		let vals = [values[2], values[3], values[4], values[13], values[14], values[15]];
		console.log(vals);
		axios({
				method: 'post',
				url: 'server',
				data: {
					alias: this.alias,
					aliasPassword: this.aliasPassword,
					passphrase: this.passphrase,
					commands: data,
					values: vals, 
					password: this.props.route.getPassword()[0],
					username: this.props.route.getPassword()[1],
				},
			})
			.then((response) => {
				if(response.status == 200) {
					alert('success');
					// clearForm();
				}
			})
			.catch((error) => {
			    console.log(error);
			});
	}

	render() {
		return (
			<div>
				<div id="serverModal" className="modal fade" role="dialog">
				  <div className="modal-dialog">
				    <div className="modal-content">
				      <div className="modal-body">
				        <Form disabled={['', '', '']} values={['', '', '']} handler={this.getAlias.bind(this)} data={forms.serverModal}></Form>
				      </div>
				    </div>
				  </div>
				</div>
				<center><h3>Server</h3></center>
				<Form disabled={this.disabled} values={this.values} data={this.formData} handler={this.sendDetails.bind(this)}></Form>
			</div>
		);
	}
}

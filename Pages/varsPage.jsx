import React from 'react';
import Form from './components/Form.jsx';
import axios from 'axios';
import forms from './store/forms';

export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.formData = forms.vars;
		this.disabled = ['', '', '', '', '', '', '', '', '', '', ''];
		this.values = ['', '', '', '', '', '', '', '', '', '', ''];
	}

	sendDetails(data, values, buttonEvent, clearForm) {
		console.log(this.props.route.getPassword());
		axios({
				method: 'post',
				url: 'vars',
				data: {
					commands: data,
					password: this.props.route.getPassword()[0],
					username: this.props.route.getPassword()[1],
				},
			})
			.then((response) => {
				if(response.status == 200) {
					this.props.router.push('server/');
					// clearForm();
				}
			})
			.catch((error) => {
			    console.log(error);
			});
	}

	// componentDidMount() {
 //    	$('[data-toggle="tooltip"]').tooltip(); 
	// }

	render() {
		return (
			<div>
				<center><h3>Vars File</h3></center>
				<Form disabled={this.disabled} values={this.values} data={this.formData} handler={this.sendDetails.bind(this)}></Form>
			</div>
		);
	}
}

import React from 'react';
export default class MultiFieldStaticCheck extends React.Component {
	constructor(props) {
		super(props);
		this.data = {};
		this.state = {
			truthTable: ['', ''],
			checked: false
		}
	}

	getData(e) {
		let el = e.target, truthTable = this.state.truthTable;
		if (el.getAttribute('type') == 'checkbox') {
			this.data[el.getAttribute('id')] = el.checked;
			if (el.checked) {
				this.data[1] = '', this.data[2] = '';
			}
			this.setState({ checked: el.checked });
		} else {
			this.data[el.getAttribute('id')] = el.value;
			truthTable[el.getAttribute('id') - 1] = this.props.check[el.getAttribute('id') - 1](el.value);
			this.setState({ truthTable: truthTable });
		}
		if (Object.keys(this.data).length == 3 && this.state.truthTable[0] && this.state.truthTable[1]) {
			this.props.updateState(this.props.index, this.props.commander(this.props.line, this.data[0], this.data[1], this.data[2]), true, el);
		}
	}

	render() {
		let isCorrect = [], icon = [], truthTable = this.state.truthTable, disabled = 'disabled';
		for (let i = 0; i < truthTable.length; i++) {
			switch(truthTable[i]) {
				case true: isCorrect.push('has-success');
						icon.push(<span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>);
						break;

				case false: isCorrect.push('has-warning');
						icon.push(<span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>);
						break;

				default: isCorrect.push('');
						 icon.push('');
			}
		}
		if (this.state.checked) {
			disabled = '';
		} else {
			disabled = 'disabled';
			icon[0] = '', icon[1] = '', isCorrect[0] = '', isCorrect[1] = '';
		}
		return (
			<div data-toggle='tooltip' title={this.props.tooltip}>
				<label>{this.props.label[0]}</label>
				<div className="row">
					<label className="col-lg-2">
					  <input id='0' type="checkbox" onChange={this.getData.bind(this)} />Activate
					</label>
					<div className={'col-lg-5 form-group has-feedback ' + isCorrect[0] }>
						<input id='1' placeholder={this.props.label[1]} onBlur={this.getData.bind(this)} className='form-control' type={this.props.label[3] ? this.props.label[3] : "text"} disabled={disabled}/>
						{icon[0]}
					</div>
					<div className={'col-lg-5 form-group has-feedback ' + isCorrect[1] }>
						<input id='2' placeholder={this.props.label[2]} onBlur={this.getData.bind(this)} className='form-control' type={this.props.label[4] ? this.props.label[4] : "text"} disabled={disabled}/>
						{icon[1]}
					</div>
				</div>
			</div>
		);
	}
}
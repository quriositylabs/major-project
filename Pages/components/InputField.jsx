import React from 'react';
export default class InputField extends React.Component{

	handleChange(e) {
		let element = e.target;
		if (element.getAttribute('type')) {
			switch(element.getAttribute('type')) {
				
				case 'radio':
				case 'radio-inline': {
					let command = this.props.commander(this.props.line, element.nextSibling.nextSibling.textContent);
					this.props.updateState(this.props.index, command, true, element.nextSibling.nextSibling.textContent);
					break;
				}

				default: {
					let truthValue = this.props.check(element.value);
					let command = this.props.commander(this.props.line, element.value);
					this.props.updateState(this.props.index, command, truthValue, element.value);
					break;
				}
			}
		}
		else {
			switch(element.tagName) {
				case 'SELECT': {
					this.props.updateState(element.previousSibling.textContent, element.value, true, element);
				}
			}
		}	
	}

	render() {
		switch(this.props.type) {

			case 'select-list': {
				var list = [];
				for ( var i = 1; i < this.props.label.length; i++) {
					list.push(<option key={i.toString()}>{this.props.label[i]}</option>);
				}
				return (
					<div data-placement='bottom' data-toggle='tooltip' title={this.props.tooltip} className="form-group">
					  <label>{this.props.label[0]}</label>
					  <select onBlur={this.handleChange.bind(this)} className="form-control">
					    {list}
					  </select>
					</div>
				);
				break;
			}

			case 'radio':
			case 'radio-inline': {
				let list = [];
				for ( let i = 1; i < this.props.label.length; i++) {
					if (this.props.label[i] == this.props.values) {
						list.push(<label key={i.toString()} className={this.props.type}><input onBlur={this.handleChange.bind(this)} name={this.props.label[0] + 'option'} type={this.props.type.split('-')[0]} checked='checked' disabled={this.props.disabled}/>{this.props.label[i]}</label>);
					} else {
						list.push(<label key={i.toString()} className={this.props.type}><input onBlur={this.handleChange.bind(this)} name={this.props.label[0] + 'option'} type={this.props.type.split('-')[0]} disabled={this.props.disabled}/>{this.props.label[i]}</label>);
					}
				}
				return (
					<div data-placement='bottom' data-toggle='tooltip' title={this.props.tooltip}>
					  <label>{this.props.label[0]}</label>
					  {list}
					</div>
				);	
				break;
			}

			case 'comment': {
				let isCorrect = '', icon;
				switch(this.props.truthValue) {
					case true: isCorrect = 'has-success';
							icon = <span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>;
							break;

					case false: isCorrect = 'has-warning';
							icon = <span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>
							break

					default: isCorrect = '';
							  icon = '';
				}
				return (
					<div data-placement='bottom' data-toggle='tooltip' title={this.props.tooltip} className={'form-group has-feedback ' + isCorrect}>
						<textarea data-toggle='tooltip' title={this.props.tooltip} onChange={this.handleChange.bind(this)} type={this.props.type} className='form-control my-form-control' placeholder={this.props.label}/>
						{icon}
					</div>
				);
			}

			default: {
				let isCorrect = '', icon;
				// truthValue log
				// console.log(this.props.truthValue);
				switch(this.props.truthValue) {
					case true: isCorrect = 'has-success';
							icon = <span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>;
							break;

					case false: isCorrect = 'has-warning';
							icon = <span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>
							break

					default: isCorrect = '';
							  icon = '';
				}
				return (
					<div data-placement='bottom' data-toggle='tooltip' title={this.props.tooltip} className={'form-group has-feedback ' + isCorrect}>
						<input onChange={this.handleChange.bind(this)} type={this.props.type} className='form-control my-form-control' placeholder={this.props.values.length != 0 ? this.props.values : this.props.label} disabled={this.props.disabled}/>
						{icon}
					</div>
				);
			} 
		}
	}
}
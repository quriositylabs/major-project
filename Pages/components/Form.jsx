// generate the layout of a form with Standard Bootstrap classes(no theme) given an object with two arrays:
// 1. type 2. label
// type array has the type of each element.
// label array's element is either a string or an array of string depending upon the type of InputField	

import React from 'react';
import InputField from './InputField.jsx';
import MultiFieldStatic from './MultiFieldStatic.jsx';
import MultiFieldStaticCheck from './MultiFieldStaticCheck.jsx';
import MultiFieldDynamic from './MultiFieldDynamic.jsx';

export default class Form extends React.Component{

	constructor(props) {
		super(props);
		this.data = [];
		this.inputs = [];
		this.status = [];
		this.state = {
			modified: false,
			filled: false
		};
	}

	formCheck() {
		let correctFields = 0;
		for (let field in this.status) {
			if(this.status[field]) {
				correctFields++;
			}
			else {
				break;
			}
		}
		return correctFields == this.props.data.type.length;
	}

	toCamel(s) {
		let res = '', word = false;
		for (let i = 0; i < s.length; ++i) {
			if (s[i] == ' ') {
				word = true;
			}

			else if (s[i] == s[i].toUpperCase()) {
				if (word) {
					res += s[i]
					word = false;
				}
				else {
					res += s[i].toLowerCase();
				}
			}

			else if (s[i] == s[i].toLowerCase()){
				if (word) {
					res += s[i].toUpperCase();
					word = false;
				}
				else {
					res += s[i];
				}
			}

			else {}
		}
		return res;
	}

	updateState(key, command, status, element) {
		if (this.data[key] !== undefined) {
			this.data[key] = command;
			this.inputs[key] = element;
			this.status[key] = status;
		} else {
			for (var i = 0; i < key; i++) {
				if (this.data[i] == undefined) {
					this.data[i] = '', this.inputs[i] = '', this.status[i] = '';
				}
			}
			this.data.splice(key, 0, command);
			this.inputs.splice(key, 0, element);
			this.status.splice(key, 0, status);			
		}
		let state = !this.state.modified;
		// command log
		console.log(this.data);
		if (this.formCheck())
			this.setState({
				status: state,
				filled: true
			});
		else
			this.setState({
				status: state,
				filled: false
			});
	}

	clearForm() {
		let state = !this.state.modified;
		for (let input in this.inputs) {
			this.inputs[input].value = null;
		}
		this.data = {};
		this.inputs = {};
		this.status = {};
		this.setState({
			modified: state,
			filled: false
		});
	}

	clickHandler(e) {
		if (this.formCheck() || 1) {
			this.props.handler(this.data, this.inputs, e, this.clearForm.bind(this));
		}
	}

	render() {
		let data = this.props.data, list = [];
		// validation log
		// console.log(this.status);
		for (let i = 0; i < data.type.length; i++) {
			let truthValue;
			switch(this.status[i]) {
				case true: truthValue = true;
						   break;

				case false: truthValue = false;
							break;

				default: 
			}
			switch(data.type[i]) {
				case 'multi-static-check': {
					list.push(<MultiFieldStaticCheck tooltip={data.tooltips[i]} line={data.lines[i]} commander={data.commanders[i].bind(this)} truthValue={truthValue} check={data.validate[i]} index={i.toString()} key={i.toString()} updateState={this.updateState.bind(this)} label={data.label[i]} />);
					break;	
				}

				case 'multi-static': {
					list.push(<MultiFieldStatic tooltip={data.tooltips[i]} line={data.lines[i]} commander={data.commanders[i].bind(this)} truthValue={truthValue} check={data.validate[i]} index={i.toString()} key={i.toString()} updateState={this.updateState.bind(this)} label={data.label[i]} />);
					break;
				}

				case 'multi-dynamic': {
					list.push(<MultiFieldDynamic tooltip={data.tooltips[i]} line={data.lines[i]} commander={data.commanders[i].bind(this)} truthValue={truthValue} check={data.validate[i]} index={i.toString()} key={i.toString()} updateState={this.updateState.bind(this)} label={data.label[i]} />);
					break;
				}

				default: {
					list.push(<InputField disabled={this.props.disabled[i]} values={this.props.values[i]} line={data.lines[i]} tooltip={data.tooltips[i]} commander={data.commanders[i].bind(this)} truthValue={truthValue} check={data.validate[i].bind(this)} index={i.toString()} key={i.toString()} updateState={this.updateState.bind(this)} type={data.type[i]} label={data.label[i]} />);
					break;	
				}
			}
		}
		return (
			<div className={data.position}>
				<h3 className='formHeading'>
					{data.heading}
				</h3>
				<hr />
				{list}
				<div className='pull-right'><button onClick={this.clickHandler.bind(this)} className={this.state.filled ? 'btn btn-primary' : 'btn btn-danger'} id='submit'>{data.buttonText}</button></div>
			</div>
		);
	}
}
import React from 'react';
export default class MultiFieldDynamic extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 1,
			truthTable: []
		};	// count of the fields
		this.data = [];				// actual data
		this.toSend = [];			// only contain the data of fully filled rows, don't have partial rows
	}

	// if atleast a single row is filled
	countArray() {
		let rows = 0, col;
		for (let i = 0; i < this.data.length; i++) {
			col = 0;
			for (let j = 0; j < this.data[i].length; j++) {
				if (this.data[i][j].length > 0 && this.state.truthTable[i][j]) col++;
			}
			if (col == this.props.label.length - 1){
				this.toSend.push(this.data[i]);
				rows++;
			};
		}
		if (rows != 0)
			return true;
		else
			return false;
	}

	// getting the data and organize it 
	getData(e) {
		let col = e.target.getAttribute('id'), row = e.target.parentNode.parentNode.getAttribute('id');
		let count = this.state.count, truthTable = this.state.truthTable;
		if (this.data[row] !== undefined) {
			if (this.data[row][col] !== undefined) {
				this.data[row][col] = e.target.value;
				truthTable[row][col] = this.props.check[col](e.target.value);
			} else {
				for (let i = 0; i < col; i++) {
					if (this.data[row][i] == undefined) {
						this.data[row][i] = '';
						truthTable[row][i] = '';
					}
				}				
				this.data[row].splice(col, 0, e.target.value);
				truthTable[row].splice(col, 0, this.props.check[col](e.target.value));
			}
		} else {
			for (let i = 0; i < row; i++) {
				if (this.data[i] == undefined) {
					this.data[i] = [];
					truthTable[i] = [];
				}
			}
			this.data.splice(row, 0, []);
			truthTable.splice(row, 0, []);
			for (let i = 0; i < col; i++) {
				if (this.data[row][i] == undefined) {
					this.data[row][i] = '';
					truthTable[row][i] = '';
				}
			}
			this.data[row].splice(col, 0, e.target.value);
			truthTable[row].splice(col, 0, this.props.check[col](e.target.value));
		}

		this.setState({
			count: count,
			truthTable: truthTable
		});

		if (this.countArray()) {
			this.props.updateState(this.props.index, this.props.commander(this.props.line, this.toSend), true, e.target);
			this.toSend = [];
		}
	}

	add(e) {
		this.setState({ count: this.state.count+1});
	}

	delete(e) {
		if (this.state.count > 1) {
			this.data.pop();
			this.setState({ count: this.state.count-1});
		}
	}

	render() {
		let rows = [], row = [], length = "col-lg-" + 12/(this.props.label.length - 1);

		let isCorrect, icon, truthTable = this.state.truthTable, count = this.state.count;
		for (let i = 0; i < count; i++) {
			row = [];
			for (let j = 0; j < this.props.label.length - 1; j++) {
				try {
					switch(truthTable[i][j]) {
						case true: {
							isCorrect = 'has-success';
							icon = (<span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>);
							break;
						}

						case false: {
							isCorrect = 'has-warning';
							icon = (<span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>);
							break;
						}

						default: {
							icon = '', isCorrect = '';
							break;
						}
					}
				}
				catch (err) {
					icon = '', isCorrect = '';
				}
				row.push(<div key={j.toString()} className={length + ' form-group has-feedback ' + isCorrect }><input placeholder={this.props.label[j+1]} id={j.toString()} onBlur={this.getData.bind(this)} type="text" className='form-control'/>{icon}</div>);
			}
			rows.push(
				<div key={i.toString()} id={i.toString()} className="row">
					{ row }
				</div>
			);
		}

		return (
			<div data-toggle='tooltip' title={this.props.tooltip}>
				<label>{this.props.label[0]}</label>
				<button className='btn btn-primary pull-right' onClick={this.add.bind(this)}>Add</button>
				<button className='btn btn-danger pull-right' onClick={this.delete.bind(this)}>Delete</button>
				<hr />
				{rows}
			</div>
		);
	}
}
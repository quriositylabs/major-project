import React from 'react';

export default class ListItem extends React.Component{

	constructor(props) {
		super(props);
		this.updated = {};
		this.fields = new Set();
	}

	onUnselectHandler(e) {
		this.props.onSelectHandler(this.props.item.id, 0, e.target);
	}	

	onSelectHandler(e) {
		this.props.onSelectHandler(this.props.item.id, 1, e.target);
	}

	onHighlightHandler(e) {	
		this.props.onHighlightHandler(this.props.item.id, 2, e.target);	
	}

	onUnhighlightHandler(e) {	
		this.props.onHighlightHandler(this.props.item.id, 0, e.target);	
	}

	normalizeHandler(e) {
		this.props.onHighlightHandler(this.props.item.id, 0, e.target);	
	}

	updateHandler(e) {		
		if (e.target.value != '') {
			this.updated[e.target.id] = e.target.value;
			if (!this.fields.has(e.target)) 
				this.fields.add(e.target);			
		}
		else {
			this.updated[e.target.id] = e.target.placeholder;
			if (this.fields.has(e.target)) 
				this.fields.delete(e.target);
		}
		this.props.updateHandler(this.props.item.id, this.updated);
	}

	clearAll() {
		let fields = Array.from(this.fields);
		for (let field in fields) {
			fields[field].value = '';
			fields[field].placeholder = this.updated[fields[field].id];
		}
		this.updated = {};
		this.fields.clear();
	}

	emptyHandler(e) {}

	render() {
		let tile, title, row, footer, click, dblclick, disabled;
		switch (this.props.stage) {
			case 0: tile = 'tile';
					title = 'tile-title';
					row = 'tile-row';
					footer = 'tile-footer';
					click = this.onSelectHandler.bind(this);
					dblclick = this.onHighlightHandler.bind(this);
					disabled = 'disabled';
					this.clearAll();
					break;

			case 1: tile = 'tile tile-clicked';
					title = 'tile-title tile-title-clicked';
					row = 'tile-row tile-row-clicked';
					footer = 'tile-footer tile-footer-clicked';
					click = this.onUnselectHandler.bind(this);
					disabled = 'disabled';
					break;

			case 2: tile = 'tile tile-highlighted';
					title = 'tile-title tile-title-highlighted';
					row = 'tile-row tile-row-highlighted';
					footer = 'tile-footer tile-footer-highlighted';
					dblclick = this.onUnhighlightHandler.bind(this);
					disabled = '';
					break;
		}

		return (
			<div onClick={click} onDoubleClick={dblclick} className={tile}>
				<input onChange={this.updateHandler.bind(this)} id='firstName' className={title} placeholder={this.props.item['firstName'].toUpperCase()} disabled={disabled}/>
				<input onChange={this.updateHandler.bind(this)} id='lastName' className={row} placeholder={this.props.item['lastName']} disabled={disabled}/>
				<input onChange={this.updateHandler.bind(this)} id='email' className={row} placeholder={this.props.item['email']} disabled={disabled}/>
				<input onChange={this.updateHandler.bind(this)} id='contactNumber' className={row} placeholder={this.props.item['contactNumber']} disabled={disabled}/>
				<input onChange={this.updateHandler.bind(this)} id='dob' className={footer} placeholder={this.props.item['dob']} disabled={disabled}/>
			</div>
		);
	}	
}
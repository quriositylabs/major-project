import React from 'react';
import ListItem from './listItem.jsx';

export default class List extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			modified: false
		}
		this.selected = new Set();
		this.highlighted = new Set();
	}

	onHighlightHandler(tileData, value, tile) {
		let state = !this.state.modified;
		if (value == 2) {
			this.highlighted.add(tileData);
		}
		else {
			this.highlighted.delete(tileData);
		}
		if (!this.props.updateActive) {
			this.props.highlightHandler();
		}
		this.setState({ modified: state });
	}

	onSelectHandler(tileData, value, tile) {
		let state = !this.state.modified;
		if (value == 1) {
			this.selected.add(tileData);
		}
		else {
			this.selected.delete(tileData);
		}
		this.props.selectHandler(tileData, value, tile);
		this.setState({ modified: state });
	}

	updateHandler(id, data) {
		this.props.updateHandler(id, data);
	} 

	render() {
		let list1 = [], list = this.props.list;
		if (!this.props.updateActive)
			this.highlighted.clear();
		if (!this.props.deleteActive)
			this.selected.clear();
		for (let i = list.length-1; i >= 0; i--) {
			let stage = 0;
			if (this.selected.has(list[i].id))
				stage = 1;
			if (this.highlighted.has(list[i].id))
				stage = 2;
			list1.push(<div className="col-lg-4 col-md-4 col-sm-6 col-xs-6"><ListItem updateHandler={this.updateHandler.bind(this)} onSelectHandler={this.onSelectHandler.bind(this)} onHighlightHandler={this.onHighlightHandler.bind(this)} key={list[i].emailId} item={list[i]} stage={stage}/></div>);
		}
		return (
			<div className="row">
				{list1}
			</div>
		);
	}
}
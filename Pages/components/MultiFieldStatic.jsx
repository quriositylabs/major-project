import React from 'react';
export default class MultiFieldStatic extends React.Component {
	constructor(props) {
		super(props);
		this.data = [];
		this.state = {
			truthTable: ['', '']
		}
	}

	getData(e) {
		let el = e.target, truthTable = this.state.truthTable;
		this.data[el.getAttribute('id')] = el.value;
		truthTable[el.getAttribute('id')] = this.props.check[el.getAttribute('id')](el.value);
		this.setState({ truthTable: truthTable });
		if (this.state.truthTable[0] && this.state.truthTable[1]) {
			this.props.updateState(this.props.index, this.props.commander(this.props.line, this.data[0], this.data[1]), true, el);
		}
	}

	render() {
		let isCorrect = [], icon = [], truthTable = this.state.truthTable;
		for (let i = 0; i < truthTable.length; i++) {
			switch(truthTable[i]) {
				case true: isCorrect.push('has-success');
						icon.push(<span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>);
						break;

				case false: isCorrect.push('has-warning');
						icon.push(<span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>);
						break;

				default: isCorrect.push('');
						 icon.push('');
			}
		}
		return (
			<div data-toggle='tooltip' title={this.props.tooltip}>
				<label>{this.props.label[0]}</label>
				<div className="row">
					<div className={'col-lg-5 form-group has-feedback ' + isCorrect[0] }>
						<input id='0' placeholder={this.props.label[1]} onBlur={this.getData.bind(this)} className='form-control' type="text"/>
						{icon[0]}
					</div>
					<div className={'col-lg-5 form-group has-feedback ' + isCorrect[1] }>
						<input id='1' placeholder={this.props.label[2]} onBlur={this.getData.bind(this)} className='form-control' type="text"/>
						{icon[1]}
					</div>
				</div>
			</div>
		);
	}
}
import React from 'react';
import Form from './components/Form.jsx';
import { Link } from 'react-router';

export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.data = [];
	}

	handleBlur(e) {
		this.data[e.target.getAttribute('id')] = e.target.value;
	}

	sendDetails(e) {
		this.props.route.updateDetails(this.data);
		this.props.router.push(e.target.getAttribute('id'));
	}

	// componentDidMount() {
	// 	$('[data-toggle="tooltip"]').tooltip();
	// }

	render() {
		return (
			<div>
				<center><h3>Enter Details</h3></center><hr />
				<div title='Your Linux Username, the configuration files should contain in this directory' data-placement='bottom' data-toggle="tooltip" className="form-group">
					<input onBlur={this.handleBlur.bind(this)} id='1' type="text" placeholder='Linux Username' className="form-control"/>
				</div>
				<div title='Your Root Password. Generate A root password first by running "sudo passwd" if you have not done already' data-placement='bottom' data-toggle="tooltip" className="form-group">
					<input onBlur={this.handleBlur.bind(this)} id='0' type="password" placeholder='Root Password' className="form-control"/>
				</div>
				<button onClick={this.sendDetails.bind(this)} className="btn btn-primary" id='vars/' >Server</button>
				<button onClick={this.sendDetails.bind(this)} className="btn btn-primary pull-right" id='client/' >Client</button>
			</div> 
		);
	}
}
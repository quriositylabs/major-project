import commanders from './commanders';
import checks from './checks';

module.exports = {
	client: {
		'label' : [['interface','tun','tap'], 'TAP Adapter Name', ['Protocol','tcp','udp'],["Remote", 'IP Address/Domain Name', 'Port Number'],['Choose a random remote host', 'Yes', 'No'],'Number of resolve Entries',['Specific Binding', 'Yes', 'No'],['Downgrade Permissions','user','group'],['Persist Options','Yes','No'],['proxy settngs','IP','port'],['Duplicate Packet Warning', 'Yes', 'No'], ['Enable HMAC firewall','Yes','No'], ['Select a Cipher','Blowfish','AES','Triple-DES'], ['Enable compression on VPN link','Yes','No'],['Logging', '1', '2', '3', '4', '5', '6', '7', '8', '9'],'Limit Repeating Messages'],
		'type' : ['radio','text','radio',"multi-dynamic",'radio','text','radio','multi-static-check','radio','multi-static-check','radio','radio','radio','radio','radio-inline','text'],
		'validate' : [checks.empty, checks.empty, checks.empty, [checks.ip, checks.port], checks.empty, checks.Numeric, checks.empty, [checks.varName, checks.varName], checks.empty, [checks.ip, checks.port], checks.empty, checks.empty, checks.empty, checks.empty, checks.empty, checks.RepeatingMessage],
		'lines' : ['','dev\-node .*','',['remote ',"remote .* [0-9]*$"],'remote-random','resolv-retry .*','nobind',['user [a-z]*','group [a-z]*'],['persist-key','persist-tun'],'http-proxy .*$','mute-replay-warnings','tls\-.*','','comp\-.*','verb [0-9]','mute [0-9]*'],
		'commanders' : [commanders.Interface, commanders.Text, commanders.Proto, commanders.MultiDynamic, commanders.Boolean, commanders.Text, commanders.BooleanInverted, commanders.MultiStaticCheck, commanders.PersistCheck, commanders.ProxyCheck, commanders.BooleanInverted, commanders.Boolean,commanders.CipherClient,commanders.Boolean, commanders.Verbo, commanders.Text],
		'tooltips': [ "Use the same setting as you are using on the server. On most systems, the VPN will not function unless you partially or fully disable the firewall for the TUN/TAP interface.", " Windows needs the TAP-Windows adapter name from the Network Connections panel if you have more than one.  On XP SP2, you may need to disable the firewall for the TAP adapter.", "Are we connecting to a TCP or UDP server?  Use the same setting as on the server.", "The hostname/IP and port of the server. You can have multiple remote entries to load balance between the servers.", "Choose a random host from the remote list for load-balancing.  Otherwise try hosts in the order specified.", "Keep trying indefinitely to resolve the host name of the OpenVPN server.  Very useful on machines which are not permanently connected to the internet such as laptops.", "Most clients don't need to bind to specific local port number." ,"Downgrade privileges after initialization (non-Windows only)", "Try to preserve some state across restarts." , "If you are connecting through an HTTP proxy to reach the actual OpenVPN server, put the proxy server/IP and port number here.  See the man page if your proxy server requires authentication.", "Wireless networks often produce a lot of duplicate packets.  Set this flag to silence duplicate packet warnings.", "If a tls-auth key is used on the server then every client must also have the key." , "Select a cryptographic cipher. If the cipher option is used on the server then you must also specify it here." , "Enable compression on the VPN link. Don't enable this unless it is also enabled in the server config file.", "Set log file verbosity.", "Silence repeating messages"],
		'buttonText' : 'Go',
		'url' : 'formdata'
	},
	
	server: {
	    'label' : ['Local IP Address for Listening', 'Port Number for TCP/UDP', ['Protocol','tcp','udp'], ['Interface','tap','tun'], 'TAP Adapter Name', ['Diffie Hellman Key Size','1024','2048'], ['Server Mode give VPN subnet','IP','Netmask'], ['Server mode for ethernet bridging','IP','Netmask','Starting IP','Ending IP'], ['Add routes for Clients to access subnets','IP address','Netmask'], ['Enable different firewall access policies','Yes','No'], ['Redirect Client(s) default gateway via VPN','Yes','No'], ['Should Clients \"see\" each other','Yes','No'], ['Keepalive','after how many seconds do ping','how many seconds after which assume remote peer down'], ['Enable HMAC firewall','Yes','No'], ['Select a Cipher','Blowfish','AES','Triple-DES'], ['Enable compression on VPN link','Yes','No'], 'Maximum concurrently connected clients to be allowed', ['Downgrade OpenVpn daemon privileges','user name','group name'], ['Method of storing log','log','log append','default'], ['Verbose level for log file','0','1','2','3','4','5','6','7','8','9'],'Silence repeating messages'],
	    'type' : ['text', 'text', 'radio','radio','text','radio','multi-static','multi-dynamic','multi-dynamic','radio','radio','radio','multi-static','radio','radio','radio','text','multi-static-check','radio','radio-inline','text'],
	    'validate' : [checks.ip, checks.port, checks.empty, checks.empty, checks.empty, checks.empty, [checks.ip, checks.mask], [checks.ip, checks.mask, checks.ip, checks.ip], [checks.ip, checks.mask], checks.empty, checks.empty, checks.empty, [checks.keepAlive, checks.keepAlive], checks.empty, checks.empty, checks.empty, checks.maximumClients, [checks.empty, checks.empty], checks.empty, checks.empty, checks.RepeatingMessage],
	    'lines' : ['local .*','port [0-9]*$', '','','dev\-node .*','dh .*', 'server [0-9\.]* [0-9\.]*$', ['server-bridge ', 'server\-bridge [0-9\.]* [0-9\.]* [0-9\.]* [0-9\.]*$'],['push "route ' ,'push \"ro.*'],'learn\-address .*$','push \"re.*','client\-t.*','keepalive [0-9][0-9]* [0-9][0-9]*','tls\-.*','','comp\-.*','max\-clients .*',['user .*','group .*'],['log   ','log-append'],'verb [0-9]','mute [0-9][0-9]*'],
	    'commanders' : [commanders.Text, commanders.Text, commanders.Proto, commanders.Interface, commanders.Text, commanders.DH, commanders.MultiStatic, commanders.MultiDynamic, commanders.MultiDynamic, commanders.Boolean, commanders.Boolean, commanders.Boolean, commanders.KeepaliveCheck, commanders.Hmac, commanders.Cipher, commanders.Boolean, commanders.Text, commanders.MultiStaticCheck, commanders.Logging, commanders.Verbo, commanders.Text],
	    'tooltips': ['Which local IP address should OpenVPN listen on? (optional)', 'Which TCP/UDP port should OpenVPN listen on? If you want to run multiple OpenVPN instances on the same machine, use a different port number for each one. You will need to open up this port on your firewall', 'TCP or UDP server?', '"dev tun" will create a routed IP tunnel, "dev tap" will create an ethernet tunnel. Use "dev tap0" if you are ethernet bridging and have precreated a tap0 virtual interface and bridged it with your ethernet interface. If you want to control access policies over the VPN, you must create firewall rules for the the TUN/TAP interface. On non-Windows systems, you can give an explicit unit number, such as tun0. On Windows, use "dev-node" for this. On most systems, the VPN will not function unless you partially or fully disable the firewall for the TUN/TAP interface.', 'Windows needs the TAP-Windows adapter name from the Network Connections panel if you have more than one.  On XP SP2 or higher, you may need to selectively disable the Windows firewall for the TAP adapter. Non-Windows systems usually don\'t need this.', ' Diffie hellman parameters.', 'Configure server mode and supply a VPN subnet for OpenVPN to draw client addresses from. The server will take 10.8.0.1 for itself, the rest will be made available to clients. Each client will be able to reach the server on 10.8.0.1. Comment this line out if you are ethernet bridging. See the man page for more info.', 'Configure server mode for ethernet bridging. You must first use your OS\'s bridging capability to bridge the TAP interface with the ethernet NIC interface.  Then you must manually set the IP/netmask on the bridge interface, here we assume 10.8.0.4/255.255.255.0.  Finally we must set aside an IP range in this subnet (start=10.8.0.50 end=10.8.0.100) to allocate to connecting clients.  Leave this line commented out unless you are ethernet bridging.', ' Push routes to the client to allow it to reach other private subnets behind the server.  Remember that these private subnets will also need to know to route the OpenVPN client address pool (10.8.0.0/255.255.255.0) back to the OpenVPN server.', 'Suppose that you want to enable different firewall access policies for different groups of clients.  There are two methods: (1) Run multiple OpenVPN daemons, one for each	group, and firewall the TUN/TAP interface for each group/daemon appropriately.(2) (Advanced) Create a script to dynamically modify the firewall in response to access from different clients.  See man page for more info on learn-address script.',  'If enabled, this directive will configure all clients to redirect their default network gateway through the VPN, causing all IP traffic such as web browsing and and DNS lookups to go through the VPN (The OpenVPN server machine may need to NAT the TUN/TAP interface to the internet in order for this to work properly). CAVEAT: May break client\'s network config if client\'s local DHCP server packets get routed through the tunnel.  Solution: make sure client\'s local DHCP server is reachable via a more specific route than the default route of 0.0.0.0/0.0.0.0.', 'Uncomment this directive to allow different clients to be able to "see" each other. By default, clients will only see the server. To force clients to only see the server, you will also need to appropriately firewall the server\'s TUN/TAP interface.', 'The keepalive directive causes ping-like messages to be sent back and forth over the link so that each side knows when the other side has gone down. Ping every 10 seconds, assume that remote peer is down if no ping received during a 120 second time period.', 'For extra security beyond that provided by SSL/TLS, create an "HMAC firewall" to help block DoS attacks and UDP port flooding. Generate with: openvpn --genkey --secret ta.key The server and each client must have a copy of this key. The second parameter should be \'0\' on the server and \'1\' on the clients.', ' Select a cryptographic cipher. This config item must be copied to the client config file as well.', 'Enable compression on the VPN link. If you enable it here, you must also enable it in the client config file.', ' The maximum number of concurrently connected clients we want to allow.', ' It\'s a good idea to reduce the OpenVPN daemon\'s privileges after initialization. You can uncomment this out on non-Windows systems.', 'By default, log messages will go to the syslog (or on Windows, if running as a service, they will go to the "\Program Files\OpenVPN\log" directory). Use log or log-append to override this default. "log" will truncate the log file on OpenVPN startup, while "log-append" will append to it.  Use one or the other (but not both).', 'Set the appropriate level of log file verbosity. 0 is silent, except for fatal errors 4 is reasonable for general usage 5 and 6 can help to debug connection problems 9 is extremely verbose', ' Silence repeating messages.  At most 20 sequential messages of the same message category will be output to the log.'],
	    'buttonText' : 'Go',
	    'url' : 'server'
	},

	vars: {
		'label':[ ['Information Of Remote CA', 'IP Address', 'Root Password', 'text', 'password'], ['Diffe-Hellman Parameter Size','1024','2048'],'CA Expiry Period in Days','Key Expiry Period in Days','Country Code','Province Code','City','Organization Name','Email','Organizational Unit Name','Key Name'],
		'type':['multi-static-check', 'radio', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text','text'],
		'validate':[[checks.ip, checks.empty], checks.empty, checks.Numeric, checks.Numeric, checks.alpha, checks.alpha, checks.alpha, checks.alpha, checks.email, checks.alpha, checks.alpha],
		'lines':[
			'',
			'export KEY_SIZE=[0-9]*$',
			'export CA_EXPIRE=[0-9]*$',
			'export KEY_EXPIRE=[0-9]*$',
			'export KEY_COUNTRY=[\"][A-Z][A-Z][\"]$',
			'export KEY_PROVINCE=[\"][A-Z][A-Z][\"]$',
			'export KEY_CITY=[\"][A-Za-z]*[\"]$',
			'export KEY_ORG=[\"][A-Za-z]*[\"]$',
			'export KEY_EMAIL=[\"][A-Za-z0-9@]*[\"]$',
			'export KEY_OU=[\"][A-Za-z0-9@]*[\"]$',
			'export KEY_NAME=[\"][A-Za-z0-9@]*[\"]$'
		],
		'commanders': [commanders.RemoteCA, commanders.Vars, commanders.Vars, commanders.Vars, commanders.Vars, commanders.Vars, commanders.Vars,commanders.Vars, commanders.Vars, commanders.Vars, commanders.Vars],
		'tooltips': ['Select this if you want to make the CA on a separate machine', '1024 or 2048, note that 2048 may take a longer time but is more secure', 'Validity, in days, of the Certification Authority for the certificate', 'Validity, in days, of the Key for the certificate', 'A unique 2 letter code Identifying your country', 'A unique 2 letter code Identifying your province within the country', 'The name of your city', 'The name of your organization', 'Your/ Organizations email address', 'The name of your unit (department/subdivision) within your organization', 'A name for the key file .Please ensure that the key name here remains the same as in the client/server configuration file at your end'],
		'buttonText': 'Go',
		'url' : 'vars'
	},

	serverModal: {
		'position': 'container-fluid',
		'heading': 'Create A Unique Alias',
		'label': ['Unique Alias', 'Alias Password', 'Client Passphrase (recommended)'],
		'type': ['text', 'password', 'password'],
		'validate': [checks.empty, checks.password, checks.password],
		'lines': ['', '', ''],
		'commanders': [commanders.Identity, commanders.Identity, commanders.Identity],
		'tooltips': ['A unique alias to save your settings', 'Your alias password', 'This option will only take effect if you want us to handle your clients. If this passphrase is given here, every client that would want to connect to your VPN server must have this password as a permission. If left empty every client can connect without any permission'],
		'buttonText': 'Go',
		'url' : 'vars'
	},

	clientModal: {
		'position': 'container-fluid',
		'heading': 'Create A Unique Alias',
		'label': ['Alias Of Server', 'Server Passphrase'],
		'type': ['text', 'password'],
		'validate': [checks.empty, checks.password],
		'lines': ['', ''],
		'commanders': [commanders.Identity, commanders.Identity],
		'tooltips': ['The Alias of the server you want to connect', 'The passphrase of the server'],
		'buttonText': 'Go',
		'url' : 'vars'	
	}		
}
import val from 'validator';
module.exports = {
	alphaNumeric: (elem) => {
		return val.isAlphanumeric(elem);
	},

	varName: (elem) => {
		return elem.match(/^[a-zA-Z\_][a-zA-Z0-9\_]*$/) != null;
	},

	alpha: (elem) => {
		return val.isAlpha(elem);
	},

	ip: (elem) => {
	    return val.isIP(elem);
	},

	keepAlive: (elem) => {
	   return val.isInt(elem,{min:0});
	},

	mask: (elem) => {
	    return val.isIP(elem);
	},

	port: (elem) => {
	    return val.isInt(elem,{min:0});
	},

	email: (elem) => {
	    return val.isEmail(elem);    
	},

	maximumClients: (elem) => {
	    return val.isInt(elem,{min:0});
	},

	Numeric: (elem) => {
		return val.isNumeric(elem);
	},

	Verbosity: (elem) => {
	    return val.isInt(elem,{min:0,max:9});
	},

	RepeatingMessage: (elem) => {
	    return val.isInt(elem,{min:0,max:20});
	},

	empty: (elem) => {
		return true;
	},

	password: (elem) => {
		if (elem.length >= 8) {
			return true;
		}
		return false;
	}
}
module.exports = {
	Vars: (line, text) => {
		if (isNaN(text))
			text = line.split('=')[0] + '="' + text + '"';
		else
			text = line.split('=')[0] + '=' + text;
		return ["sed s/'" + line + "/" + text + "/'"];
	},

	RemoteCA: (line, check, text1, text2) => {
		if (check) {
			return [text1, text2];
		} else {
			return '';
		}
	},

	Proto: (line, text) => {
		// line empty
		if (text == 'udp') {
			return ["sed -e '/;proto udp/s/^.//'", "sed '/^proto tcp/s/^/;/'"];
		} else {
			return ["sed -e '/;proto tcp/s/^.//'", "sed '/^proto udp/s/^/;/'"];
		}
	},

	Interface: (line, text) => {
		// line empty
		if (text == 'tun') {
			return ["sed -e '/;dev tun/s/^.//'", "sed '/^dev tap/s/^/;/'"];
		} else {
			return ["sed -e '/;dev tap/s/^.//'", "sed '/^dev tun/s/^/;/'"];
		}
	},

	Text: (line, text) => {
		// line is a Regex
		let words = line.split(' '), lineStripped = '';
		for (let i = 0; i < words.length - 1; ++i) {
			lineStripped += words[i] + ' ';
		}
		return ["sed -e '/;" + line + "/s/^.//'", "sed 's/" + line + "/" + lineStripped + text + "/'"]
	},

	MultiDynamic: (lines, data) => {
		// line is a regex
		let commands = [];
		commands.push("sed '0,/" + lines[1] + "/{s/" + lines[1] + "/ZZZZZZZZ/}'");
		commands.push("sed '/" + lines[1] + "/d'");
		for (let i = 0; i < data.length; i++) {
			let line = lines[0];
			for (let j = 0; j < data[i].length; j++) {
				line += data[i][j] + ' ';
			}
			if (lines[0] == 'push "route ') {
				line = line.slice(0, -1);
				line += '"';
			}
			commands.push("sed '/ZZZZZZZZ/a\\" + line + "'");
		}
		commands.push("sed '/ZZZZZZZZ/d'");
		return commands;
	},

	MultiStatic: (lines, text1, text2) => {
		// line is a regex array 
		let commands = [];
		let words = lines.split(' ');
		commands.push("sed 's/" + lines + "/" + words[0] + " " + text1 + " " + text2 + "/'");
		return commands;
	},

	MultiStaticCheck: (lines, check, text1, text2) => {
		// line is a regex array 
		let commands = [];
		let words = lines[0].split(' '), lineStripped1 = '', lineStripped2 = '';
		for (let i = 0; i < words.length - 1; ++i) {
			lineStripped1 += words[i] + ' ';
		}
		words = lines[1].split(' ');
		for (let i = 0; i < words.length - 1; ++i) {
			lineStripped2 += words[i] + ' ';
		}
		if (check) {
			commands.push("sed -e '/;" + lines[0] + "/s/^.//'");
			commands.push("sed -e '/;" + lines[1] + "/s/^.//'");
			commands.push("sed 's/" + lines[0] + "/" + lineStripped1 + text1 + "/'");
			commands.push("sed 's/" + lines[1] + "/" + lineStripped2 + text2 + "/'");
			return commands;
		}
		else {
			// sed '/^cipher DES-EDE3-CBC/s/^/;/'
			// commands.push("sed '/^" + lines[0] + "/s/^/;/'");
			// commands.push("sed '/^" + lines[1] + "/s/^/;/'");	
			return '';
		}
	},

	Verbo: (line, text) => {
		// line is a regex "verb [0-9]"
		let words = line.split(' '), lineStripped = '';
		for (let i = 0; i < words.length - 1; ++i) {
			lineStripped += words[i] + ' ';
		}
		return ["sed 's/" + line + "/" + lineStripped + text + "/'"]	
	},

	Logging: (line, text) => {
		// line empty
		if (text == 'log') {
			return ["sed -e '/;" + line[0] + "/s/^.//'","sed '/^" + line[1] + "/s/^/;/'"];
		} else if (text == 'log append') {
			return ["sed -e '/;" + line[1] + "/s/^.//'","sed '/^" + line[0] + "/s/^/;/'"];
		} else {
			return ["sed '/^" + line[0] + "/s/^/;/'","sed '/^" + line[1] + "/s/^/;/'"];
		}
	},

	Cipher: (line, text) => {
		// line empty
		if (text == 'Blowfish') {
			return ["sed -e '/;cipher BF-CBC/s/^.//'", "sed '/^cipher AES-128-CBC/s/^/;/'", "sed '/^cipher DES-EDE3-CBC/s/^/;/'"];
		} else if (text == 'AES') {
			return ["sed -e '/;cipher AES-128-CBC/s/^.//'", "sed '/^cipher BF-CBC/s/^/;/'", "sed '/^cipher DES-EDE3-CBC/s/^/;/'"];
		} else {
			return ["sed -e '/;cipher DES-EDE3-CBC/s/^.//'", "sed '/^cipher BF-CBC/s/^/;/'", "sed '/^cipher AES-128-CBC/s/^/;/'"];
		}
	},

	DH: (line, text) => {
		// line is a Regex dh [0-9a-z\.]*
		let words = line.split(' '), lineStripped = '';
		for (let i = 0; i < words.length - 1; ++i) {
			lineStripped += words[i] + ' ';
		}
		return ["sed 's/" + line + "/" + lineStripped + "dh" +  text + ".pem" + "/'"]
	},

	Hmac: (line, bool) => {
		if (bool == 'Yes') {
			return ['openvpn --genkey --secret ta.key; cat' ,"sed -e '/;" + line + "/s/^.//'"];
		} else {
			return ["sed '/^" + line + "/s/^/;/'"];
		}	
	},

	Boolean: (line, bool) => {
		// line is static
		if (bool == 'Yes') {
			return ["sed -e '/;" + line + "/s/^.//'"];
		} else {
			return ["sed '/^" + line + "/s/^/;/'"];
		}
	},

	Identity: (line, text) => {
		return text;
	},

	KeepaliveCheck: (lines,text1,text2) => {
	    let commands = [];
	    let words = lines.split(' ');
	    commands.push("sed -e '/;" + lines + "/s/^.//'");
	    commands.push("sed 's/" + lines + "/" + words[0] +' '+text1+' '+text2 + "/'");
	    return commands;
	},

	ProxyCheck: (lines,check,text1,text2) => {
	    let commands = [];
	    let words = lines.split(' ');
	    if(check){
	        commands.push("sed -e '/;" + lines + "/s/^.//'");
	        commands.push("sed 's/" + lines + "/" + words[0] +' '+text1+' '+text2 + "/'");
	    }else{
	        commands.push("sed '/^" + lines + "/s/^/;/'");
	    }
	    return commands;
	},

	BooleanInverted: (line, bool) => {
	    // line is static
	    if (bool == 'Yes') {
	   	 return ["sed '/^" + line + "/s/^/;/'"];
	     } else {
	   	return ["sed -e '/;" + line + "/s/^.//'"];
	    }
	},

	PersistCheck: (line, bool) => {
	    // line is static
	    if (bool == 'Yes') {
	   	 return ["sed -e '/;" + line[0] + "/s/^.//'","sed -e '/;" + line[1] + "/s/^.//'"];
	    } else {
	   	 return ["sed '/^" + line[0] + "/s/^/;/'","sed '/^" + line[1] + "/s/^/;/'"];
	    }
	},

	CipherClient: (line, text) => {
		// line empty
		if (text == 'Blowfish') {
			return ["sed -e '/;cipher .*/s/^.//'","sed -e '/cipher .*/s/^cipher .*/cipher BF-CBC/'"];
		} else if (text == 'AES') {
			return ["sed -e '/;cipher .*/s/^.//'","sed -e '/cipher .*/s/^cipher .*/cipher AES-128-CBC/'"];
		} else {
			return ["sed -e '/;cipher .*/s/^.//'","sed -e '/cipher .*/s/^cipher .*/cipher DES-EDE3-CBC/'"];
		}
	}
}
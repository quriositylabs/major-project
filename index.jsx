import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Home from './Pages/Home.jsx';
import serverPage from './Pages/serverPage.jsx';
import varsPage from './Pages/varsPage.jsx';
import clientPage from './Pages/clientPage.jsx';

class Layout extends React.Component {

	render() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
}

class Page extends React.Component {

	constructor(props) {
		super(props);
		this.username = '';
		this.password = '';
	}

	updateDetails(data) {
		this.password = data[0];
		this.username = data[1];
	}

	getPassword() {
		return [this.password, this.username];
	}

	render() {
		return (
			<Router history={hashHistory}>
				<Route path="/" component={Layout}>
					<IndexRoute component={Home} updateDetails={this.updateDetails.bind(this)}></IndexRoute>
					<Route path="server/" component={serverPage} getPassword={this.getPassword.bind(this)}></Route>
					<Route path="vars/" component={varsPage} getPassword={this.getPassword.bind(this)}></Route>
					<Route path="client/" component={clientPage} getPassword={this.getPassword.bind(this)}></Route>
				</Route>	
			</Router>
		);
	}
}

ReactDOM.render(<Page/>, document.getElementById('app'));

